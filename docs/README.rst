.. _readme:

discourse
=========

Setup and configure `discourse`_ container deployed with `discourse_docker`_ repository

.. contents:: **Table of Contents**

General notes
-------------

See the full `SaltStack Formulas installation and usage instructions
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

If you are interested in writing or contributing to formulas, please pay attention to the `Writing Formula Section
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#writing-formulas>`_.

If you want to use this formula, please pay attention to the ``FORMULA`` file and/or ``git tag``,
which contains the currently released version. This formula is versioned according to `Semantic Versioning <http://semver.org/>`_.

See `Formula Versioning Section <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#versioning>`_ for more details.

More information `SALT AS A CLOUD CONTROLLER
<https://docs.saltstack.com/en/latest/topics/tutorials/cloud_controller.html>`_.

Contributing to this repo
-------------------------

**Commit message formatting is significant!!**

Please see `How to contribute <https://github.com/saltstack-formulas/.github/blob/master/CONTRIBUTING.rst>`_ for more details.

Available states
----------------

.. contents::
   :local:

``discourse``
^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

- install dependencies
- configure deployment of ``discourse`` container
- deploy the ``discourse`` application container

``discourse.config``
^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will configure the deployment of ``discourse``.


``discourse.config.tls``
^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure a personal TLS certificate and key instead
of using Let's Encrypt if the ``discourse:tls:enabled`` is ``true``
and ``discourse:letsencrypt:enabled`` is false.

It requires two private pillar values:

- ``discourse.tls.cert``
- ``discourse.tls.key``

It hooks as a dependency of ``discourse.config.file``.

``discourse.config.file``
^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure the deployement by generating the
``containers/app.yml`` file.

It depends on ``discourse.sources.hack``.

``discourse.service``
^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will rebuild the ``discourse`` application container if any
configuration file has changed and make sure the container is started.

``discourse.service.rebuild``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will rebuild the ``discourse`` application container if any
configuration file has changed.

It depends on:

- ``discourse.sources.hack``
- ``discourse.config.file``

``discourse.service.running``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will start the application container if it's not running.

It depends on ``discourse.service.rebuild``.

``discourse.setup``
^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will initialise some ``discourse`` application parameters.

``discourse.setup.settings``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will load series of settings.

``discourse.setup.settings.global``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure some ``discourse`` `site settings`_ with
values from salt configuration.

It depends on ``discourse.service.running``.

``discourse.setup.settings.oauth2``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure the ``discourse`` `OAuth2 plugin settings`_ with
values from salt configuration.

Note that the values from salt configuration are named like the
`OAuth2 pluging settings`_ except that the ``oauth2_`` prefix is
removed. See `pillar.example`_.

It depends on ``discourse.service.running``.

``discourse.setup.users``
^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will manage a set of users based on salt configuration.

``discourse.setup.users.create``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will manage a set of users based on salt configuration:

- create them if they don't exists and send password reset email
- set them active or disable them
- set them ``admin`` if configured

It depends on ``discourse.service.running``.

``discourse.setup.themes``
^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will install a list of themes.

``discourse.setup.themes.install``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will install the themes.

It depends on ``discourse.service.running``.

``discourse.setup.categories``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state will create a list of categories.

``discourse.setup.categories.create``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will create the categories.

It depends on ``discourse.service.running``.

``discourse.clean``
^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

- stop ``discourse`` container

``discourse.config.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the generated ``containers/app.yml`` deployment
configuration file.

It depends on ``discourse.service.dead``.

``discourse.service.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will stop the ``discourse`` application container.


Launcher states
^^^^^^^^^^^^^^^

The following states wrap the ``discourse`` launcher commands which
avoid to use either:

- a direct SSH connection to execute the command locally
- ``salt-ssh minion cmd.run './launcher <command> app' cwd='/srv/discourse'``

``disourse.launcher.rebuild``
`````````````````````````````

Execute the ``./laucher rebuild app`` command in the directory defined
by the ``discourse.directory`` configuration.

``disourse.launcher.bootsrap``
``````````````````````````````

Execute the ``./laucher bootstrap app`` command in the directory
defined by the ``discourse.directory`` configuration.

``disourse.launcher.start``
```````````````````````````

Execute the ``./laucher start app`` command in the directory defined
by the ``discourse.directory`` configuration.

``disourse.launcher.stop``
``````````````````````````

Execute the ``./laucher stop app`` command in the directory defined by
the ``discourse.directory`` configuration.

``disourse.launcher.restart``
`````````````````````````````

Execute the ``./laucher restart app`` command in the directory defined
by the ``discourse.directory`` configuration.

``disourse.launcher.cleanup``
`````````````````````````````

Execute the ``./laucher cleanup app`` command in the directory defined
by the ``discourse.directory`` configuration.

``disourse.launcher.destroy``
`````````````````````````````

Execute the ``./laucher destroy app`` command in the directory defined
by the ``discourse.directory`` configuration.


Utility states
^^^^^^^^^^^^^^

The following states are used by the previous states to setup the
environment.

``discourse.sources``
`````````````````````

*Meta-state (This is a state that includes other states)*.

- install the git package
- clone the latest `discourse_docker`_ repository
- install new rake tasks

``discourse.docker``
````````````````````

*Meta-state (This is a state that includes other states)*.

Install docker package and start the service.

``discourse.docker.clean``
``````````````````````````

*Meta-state (This is a state that includes other states)*.

Stop the docker service and remove the packages.

``discourse.docker.package``
````````````````````````````

This state install the docker package only.

``discourse.docker.package.clean``
``````````````````````````````````

This state remove the docker package only and depends on
``discourse.docker.service.clean``.

``discourse.docker.service``
````````````````````````````

Starts the docker service.

``discourse.docker.service.clean``
``````````````````````````````````

Disable and stop the docker service.

``discourse.netcat``
````````````````````

Install the ``netcat`` package required by the deployment of ``discourse``.

``discourse.net-tools``
```````````````````````

Install the ``net-tools`` package required by the deployment of ``discourse``.

.. _pillar.example: ../pillar.example
.. _discourse: https://www.discourse.org
.. _discourse_docker: https://github.com/discourse/discourse_docker
.. _site settings: https://github.com/discourse/discourse/blob/master/config/site_settings.yml
.. _OAuth2 plugin settings: https://github.com/discourse/discourse-oauth2-basic/blob/master/config/settings.yml
