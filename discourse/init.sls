# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .net-tools
  - .netcat
  - .docker
  - .sources
  - .config
  - .service
  - .setup
