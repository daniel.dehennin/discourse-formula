# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_file = tplroot ~ '.config.file' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

include:
  - {{ sls_config_file }}

{%- if discourse | traverse('tls:enabled', False) | to_bool %}
{%- set tls_dir = discourse.directory | path_join('shared',
                                                  'standalone',
                                                  'ssl'
                                                 )
  %}
{%- set tls = discourse | traverse('tls') %}

discourse-config-tls-force-tls-template-file-managed:
  file.managed:
    - name: {{ discourse.directory | path_join('templates',
                                               'force-tls.template.yml')
              }}
    - source: {{ files_switch(['force-tls.template.yml.jinja'],
                              lookup='discourse-config-tls-force-tls-template-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - require_in:
      - sls: {{ sls_config_file }}


discourse-config-tls-cert-file-managed:
  file.managed:
    - name: {{ tls_dir | path_join('ssl.crt') }}
    - source: {{ files_switch(['ssl.crt', 'ssl.crt.jinja'],
                              lookup='discourse-config-tls-cert-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: 600
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        tls: {{ tls | json }}
    - require_in:
      - sls: {{ sls_config_file }}

discourse-config-tls-key-file-managed:
  file.managed:
    - name: {{ tls_dir | path_join('ssl.key') }}
    - source: {{ files_switch(['ssl.key', 'ssl.key.jinja'],
                              lookup='discourse-config-tls-key-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: 600
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        tls: {{ tls | json }}
    - require_in:
      - sls: {{ sls_config_file }}
{%- endif %}
