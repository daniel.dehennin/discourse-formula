# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_dead = tplroot ~ '.service.dead' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_service_dead }}

discourse-config-clean-file-absent:
  file.absent:
    - name: {{ discourse.config }}
    - require:
      - sls: {{ sls_service_dead }}
