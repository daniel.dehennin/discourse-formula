# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_sources_hack = tplroot ~ '.sources.hack' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

include:
  - {{ sls_sources_hack }}

{%- set config = discourse.directory | path_join('containers', 'app.yml') %}

discourse-config-file-file-managed:
  file.managed:
    - name: {{ config }}
    - source: {{ files_switch(['app.yml.jinja'],
                              lookup='discourse-config-file-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        discourse: {{ discourse | json }}
    - require:
      - sls: {{ sls_sources_hack }}
