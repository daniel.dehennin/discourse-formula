# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

{% if discourse | traverse('docker:repo:use_vendor_repo', False) | to_bool %}
  {%- set docker = discourse | traverse('docker', {}) %}

  {%- set dependencies = discourse | traverse('docker:repo:dependencies', []) %}
  {%- set url = 'https://download.docker.com/linux/' ~ grains["os"]|lower ~ ' ' ~ grains["oscodename"]|lower ~ ' stable' %}
  {%- set key_url = 'https://download.docker.com/linux/' ~ grains["os"]|lower ~ '/gpg' %}

  {%- for dependency in dependencies %}
discourse-docker-repo-install-dependency-{{ dependency }}-pkg-installed:
  pkg.installed:
    - name: {{ dependency }}
  {%- endfor %}

discourse-docker-repo-repo-managed:
  pkgrepo.managed:
    - humanname: {{ grains["os"] }} {{ grains["oscodename"]|capitalize }} Docker Package Repository
    - name: deb [arch={{ grains["osarch"] }}] {{ url }}
    - file: /etc/apt/sources.list.d/docker.list
    - key_url: {{ key_url }}
    - refresh: True
{%- endif %}
