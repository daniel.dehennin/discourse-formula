# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_docker_package_install = tplroot ~ '.docker.package.install' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_docker_package_install }}

discourse-docker-service-running-service-running:
  service.running:
    - name: docker
    - enable: True
    - require:
      - sls: {{ sls_docker_package_install }}
