# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_discourse_clean = tplroot ~ '.clean' %}
{%- from tplroot | path_join("/map.jinja") import discourse with context %}

include:
  - {{ sls_discourse_clean }}

discourse-docker-service-clean-service-dead:
  service.dead:
    - name: docker
    - enable: False
    - require:
      - sls: {{ sls_discourse_clean }}
