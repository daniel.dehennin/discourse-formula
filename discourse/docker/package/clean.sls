# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_docker_service_clean = tplroot ~ '.docker.service.clean' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_docker_service_clean }}

discourse-docker-package-clean-pkg-removed:
  pkg.removed:
    - name: docker.io
    - require:
      - sls: {{ sls_docker_service_clean }}
