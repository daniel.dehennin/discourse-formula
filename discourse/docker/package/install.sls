# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

{% if discourse | traverse('docker:repo:use_vendor_repo', False) | to_bool %}
{%-   set sls_docker_repo_install = tplroot ~ '.docker.repo.install' %}
{%-   set sls_depends = [{'sls': sls_docker_repo_install}] %}
{%-   set packages = discourse | traverse('docker:packages:vendor', []) %}

include:
  - {{ sls_docker_repo_install }}
{%- else %}
{%-   set sls_depends = [] %}
{%-   set packages = discourse | traverse('docker:packages:distrib', []) %}
{%- endif %}

{%- for package in packages %}
discourse-docker-package-install-{{ package }}-pkg-installed:
  pkg.installed:
    - name: {{ package }}
    - require: {{ sls_depends }}
{%- endfor %}

{%- set compose = discourse | traverse('docker:compose') %}
discourse-docker-package-install-docker-compose-file-managed:
  file.managed:
    - name: {{ compose.name }}
    - source: {{ compose.source }}
    - source_hash: {{ compose.source_hash }}
    - user: {{ compose.owner }}
    - group: {{ compose.group }}
    - mode: {{ compose.mode }}
