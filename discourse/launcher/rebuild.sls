# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_discourse_netcat_package_install = tplroot ~ '.netcat.install' %}
{%- set sls_discourse_net_tools_package_install = tplroot ~ '.net-tools.install' %}
{%- set sls_discourse_config_file = tplroot ~ '.config.file' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_discourse_netcat_package_install }}
  - {{ sls_discourse_net_tools_package_install }}
  - {{ sls_discourse_config_file }}

discourse-launcher-rebuild-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:rebuild_cmd') }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_discourse_netcat_package_install }}
      - sls: {{ sls_discourse_net_tools_package_install }}
      - sls: {{ sls_discourse_config_file }}
