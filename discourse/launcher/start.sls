# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_rebuild = tplroot ~ '.service.rebuild' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_service_rebuild }}

discourse-launcher-start-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:start_cmd') }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_service_rebuild }}
