# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

discourse-launcher-destroy-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:destroy_cmd') }}
    - cwd: {{ discourse.directory }}
