# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_running = tplroot ~ '.service.running' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_service_running }}

{%- for user, params in discourse.get('users', {}).items() %}
  {%- set email = params.get('email', '') %}
  {%- set password = params.get('password', '') %}
  {%- set enabled = params.get('enabled', 'True') %}
  {%- set is_admin = params.get('admin', 'False') %}
  {%- set rake_cmd = 'cd /var/www/discourse/ && rake autosetup:createuser['
                     ~ '"' ~ email ~ '"'
                     ~ ',"' ~ password ~ '"'
                     ~ ',"' ~ enabled ~ '"'
                     ~ ',"' ~ is_admin ~ '"'
                     ~ ']'
    %}

discourse-setup-users-create-{{ user }}-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:run_cmd')
               ~ " '"
               ~ rake_cmd
               ~ "'"
             }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_service_running }}
    - retry:
        attempts: 5
        until: True
        interval: 2
        splay: 2
{%- endfor %}
