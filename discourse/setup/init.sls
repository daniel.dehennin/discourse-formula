# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .settings
  - .users
  - .themes
  - .categories
