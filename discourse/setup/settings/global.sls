# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_running = tplroot ~ '.service.running' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

include:
  - {{ sls_service_running }}

{%- set settings_filename = 'global.yaml' %}

{#- On the minion #}
{%- set dir = discourse.directory | path_join('shared',
                                              'standalone',
                                              'settings'
                                    )
  %}
{%- set settings_path = dir | path_join(settings_filename) %}

{#- In the container #}
{%- set in_container_dir = '/shared' | path_join('settings') %}
{%- set in_contairer_path = in_container_dir | path_join(settings_filename) %}

{%- set rake_cmd = 'cd /var/www/discourse/ '
                   ~ '&& rake site_settings:import '
                   ~ '< ' ~ in_contairer_path
  %}

{%- set settings = discourse | traverse('settings', {}) %}

discourse-setup-settings-global-file-managed:
  file.managed:
    - name: {{ settings_path }}
    - source: {{ files_switch(['settings.yaml.jinja'],
                              lookup='discourse-setup-settings-global-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        settings: {{ settings | tojson }}

discourse-setup-settings-global-rake-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:run_cmd')
               ~ " '"
               ~ rake_cmd
               ~ "'"
             }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_service_running }}
    - onchanges:
      - file: discourse-setup-settings-global-file-managed
