
# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_running = tplroot ~ '.service.running' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

{#- Do nothing if OAuth2 is not enabled #}
{% if discourse | traverse('oauth2:enabled', False) | to_bool %}
include:
  - {{ sls_service_running }}

{%- set settings_filename = 'oauth2.yaml' %}

{#- On the minion #}
{%- set dir = discourse.directory | path_join('shared',
                                              'standalone',
                                              'settings'
                                    )
%}
{%- set settings_path = dir | path_join(settings_filename) %}

{#- In the container #}
{%- set in_container_dir = '/shared' | path_join('settings') %}
{%- set in_contairer_path = in_container_dir | path_join(settings_filename) %}

{%- set rake_cmd = 'cd /var/www/discourse/ '
                   ~ '&& rake site_settings:import '
                   ~ '< ' ~ in_contairer_path
%}


{#- the configuration mix settings with formula parameters #}
{% set skip_settings = {'git-url': true} %}
{#- all oauth2 values are prefixed with `oauth2_` #}
{%- set settings = {} %}
{%- for key, value in discourse.get('oauth2', {}).items() %}
{%-   if key not in skip_settings %}
{%-     do settings.update({'oauth2_' ~ key: value}) %}
{%-   endif %}
{%- endfor %}

discourse-setup-settings-oauth2-file-managed:
  file.managed:
    - name: {{ settings_path }}
    - source: {{ files_switch(['settings.yaml.jinja'],
                              lookup='discourse-setup-settings-oauth2-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        settings: {{ settings }}

discourse-setup-settings-oauth2-import-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:run_cmd')
               ~ " '"
               ~ rake_cmd
               ~ "'"
             }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_service_running }}
    - onchanges:
      - file: discourse-setup-settings-oauth2-file-managed
{%- endif %}
