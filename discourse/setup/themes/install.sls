# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_running = tplroot ~ '.service.running' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

include:
  - {{ sls_service_running }}

{#- On the minion #}
{%- set dir = discourse.directory | path_join('shared',
                                              'standalone',
                                              'themes'
                                    )
%}

{#- In the container #}
{%- set in_container_dir = '/shared' | path_join('settings') %}

{%- set themes = discourse | traverse('themes', {}) %}

{%- for name, params in themes.items() %}
{%-   set theme_filename = name ~ '.yaml' %}
{%-   set theme_path = dir | path_join(theme_filename) %}
{%-   set in_contairer_path = in_container_dir | path_join(theme_filename) %}
{%-   set rake_cmd = 'cd /var/www/discourse/ '
                     ~ '&& rake themes:install '
                     ~ '< ' ~ in_contairer_path
%}

{%-   set theme = {'theme_name': params.get('theme_name')} %}
{%-   set config_file_lookup = 'discourse-setup-themes-install-' ~ name ~ '-install-file-managed' %}

discourse-setup-themes-install-{{ name }}-config-file-managed:
  file.managed:
    - name: {{ theme_path }}
    - source: {{ files_switch(['theme.yaml.jinja'],
                              lookup=config_file_lookup,
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        theme: {{ theme | tojson }}

discourse-setup-themes-install-{{ name }}-rake-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:run_cmd')
               ~ " '"
               ~ rake_cmd
               ~ "'"
             }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_service_running }}
    - onchanges:
      - file: discourse-setup-themes-install-{{ name }}-config-file-managed
{%- endfor %}
