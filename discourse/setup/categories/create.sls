# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_running = tplroot ~ '.service.running' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

include:
  - {{ sls_service_running }}

{%- set categories_filename = 'categories.json' %}

{#- On the minion #}
{%- set dir = discourse.directory | path_join('shared',
                                              'standalone',
                                              'categories'
                                    )
%}
{%- set categories_path = dir | path_join(categories_filename) %}

{#- In the container #}
{%- set in_container_dir = '/shared' | path_join('categories') %}
{%- set in_container_path = in_container_dir | path_join(categories_filename) %}

{%- set rake_cmd = 'cd /var/www/discourse/ '
                   ~ '&& rake import:file['
                   ~ in_container_path
                   ~ ']'
%}

{%- set categories = discourse | traverse('categories', {}) %}

{%- for category in categories %}
{%-   set name = category.get('name') %}

{#- Generate a 64 bits category `id` based on sha256 of `name` #}
{#- This will force Discourse to use the first non used `id` #}
{#- But Discourse keep this `id` internally so we can use it as `parent_cateegory_id` #}
{%-   if category.get('id', 0) == 0 %}
{%-     set id = name | sha256 | truncate(length=8, end='') | int(base=16) %}
{%-     do category.update({'id': id}) %}
{%-   endif %}

{#- Generate the 64 bits parent `id` based on sha256 of its `name` #}
{#- Remember that Discourse keep internally this generated `id`? #}
{%-   if category.get('parent', None) %}
{#-     Use pop to remove the key from the dict #}
{%-     set parent_name = category.pop('parent') %}
{%-     set parent_id = parent_name | sha256 | truncate(length=8, end='') | int(base=16) %}
{%-     do category.update({'parent_category_id': parent_id}) %}
{%-   endif %}

{%- endfor %}

{%- if categories | length > 0 %}
discourse-setup-categories-create-config-file-managed:
  file.managed:
    - name: {{ categories_path }}
    - source: {{ files_switch(['categories.json.jinja'],
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - context:
        categories: {{ {'categories': categories} | tojson }}

discourse-setup-categories-create-rake-cmd-run:
  cmd.run:
    - name: {{ discourse | traverse('launcher:run_cmd')
               ~ " '"
               ~ rake_cmd
               ~ "'"
             }}
    - cwd: {{ discourse.directory }}
    - require:
      - sls: {{ sls_service_running }}
    - onchanges:
      - file: discourse-setup-categories-create-config-file-managed
{%- endif %}
