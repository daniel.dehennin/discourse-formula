# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_sources_package_install = tplroot ~ '.sources.package.install' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}

include:
  - {{ sls_sources_package_install }}

discourse-sources-clone-git-latest:
  git.latest:
    - name: {{ discourse | traverse('docker-git-url') }}
    - target: {{ discourse.directory }}
    - require:
      - sls: {{ sls_sources_package_install }}
