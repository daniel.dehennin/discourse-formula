# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_sources_clone = tplroot ~ '.sources.clone' %}
{%- from tplroot | path_join("map.jinja") import discourse with context %}
{%- from tplroot | path_join("libtofs.jinja") import files_switch with context %}

include:
  - {{ sls_sources_clone }}

discourse-sources-hack-autosetup-template-file-managed:
  file.managed:
    - name: {{ discourse.directory | path_join('templates', 'rake.autosetup.template.yml') }}
    - source: {{ files_switch(['rake.autosetup.template.yml'],
                              lookup='discourse-sources-hack-autosetup-template-file-managed',
                              use_subpath=True
                 )
              }}
    - mode: '644'
    - user: 'root'
    - group: 'root'
    - require:
      - sls: {{ sls_sources_clone }}
