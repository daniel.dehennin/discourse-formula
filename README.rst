.. _readme:

Setup discourse with `SaltStack`_
=================================

Setup and configure `discourse`_ container deployed with `discourse_docker`_ repository.

.. contents:: **Table of Contents**

.. _deployment-using-salt-ssh:

Deployment using `salt-ssh`_
----------------------------

If your distribution does not provide a recent enough `salt-ssh`_ you can configure an `additional repository`_.

First, you need to `configure salt-ssh <#configuring-salt-ssh>`_.

To work around a `bug in salt-ssh that prevent using gitfs`_, you need to manually clone the formula.

.. code-block:: bash

    git clone https://gitlab.mim-libre.fr/la-crise/discourse-formula.git ``~/.salt/srv/formulas/``

Then, make the formula available to minions by linking it's directory in the `salt fileserver`_

.. code-block:: bash

    ln -s ~/.salt/srv/formula/discourse-formula/discourse ~/.salt/srv/salt/discourse

Create the pillar ``~/.salt/srv/pillar/discourse.sls`` to configure discourse:

.. code-block:: yaml

    # -*- coding: utf-8 -*-
    # vim: ft=yaml
    ---
    discourse:
      hostname: 'forum.example.net'
      developer_emails: 'me@example.net'
      smtp:
        address: 'smtp.example.net'
        port: '25'
        user_name: 'me@example.net'
        start_tls: true
        # the following must be set in pillar
        # smtp_password: ~
      letsencrypt:
        enabled: true
        account_email: 'me@example.net'
      settings:
        default_locale: fr
        title: This is a sample Discourse
        site_description: This is a web forum based on Discourse
        short_site_description: Web forum
        contact_email: william.shakespears@example.net
        contact_url: https://shakespears.example.net
        enable_signup_cta: 'false'
        default_trust_level: '1'
        # Disable local logins for OAuth2
        enable_local_logins: 'false'
      oauth2:
        enabled: true
        # set settings without the `oauth2_` prefix
        # https://github.com/discourse/discourse-oauth2-basic/blob/master/config/settings.yml
        client_id: sso
        client_secret: ThisIsVerySecret
        authorize_url: https://auth.example.net/auth/realms/example/protocol/openid-connect/auth
        token_url: https://auth.example.net/auth/realms/example/protocol/openid-connect/token/
        callback_user_info_paths: ~
        user_json_url: https://auth.example.net/auth/realms/apps/protocol/openid-connect/userinfo
        json_user_id_path: sub
        json_username_path: preferred_username
        json_name_path: name
        json_email_path: email
        json_email_verified_path: email_verified
        debug_auth: 'true'
        button_title: OAuth Example
        full_screen_login: 'true'
      users:
        william:
          # Password reset mail is sent by default
          email: 'william.shakespears@example.net'
          enabled: true
          admin: true
    ...


Now, associate this pillar to the minion by creating or updating ``~/.salt/pillar/top.sls``:

.. code-block:: yaml

    base:
      'forum':
        - discourse


Now, it's possible to deploy and configure the `discourse`_ application container:

.. code-block:: bash

    salt-ssh forum state.apply discourse


You can can execute ``laucher`` command directly with `salt-ssh`_ or apply ``states`` one by one, for more information on which ``states`` are available you should read the `formula documentation`_.


Configuring ``salt-ssh``
------------------------

Then, to be able to `use salt-ssh as non root user`_, you need to configure several files.

Here is the configuration `based on mine <https://gist.github.com/baby-gnu/8a198629161fe8fc60bd2b33a8321012>`_.


``Saltfile`` to tell `salt-ssh`_ where is the configuration directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create the file ``~/.salt/Saltfile`` with the following content

.. code-block:: yaml

    # -*- yaml -*-

    # Expanding ~ is working here
    salt-ssh:
      config_dir: ~/.salt


``master`` configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This file will set different parameters to tell `salt-ssh`_:

- to not create files outside our home directory
- to look for `states`_ and `pillars`_ under our home directory ``~/.salt/srv/``

.. code-block:: yaml

    # -*- mode: yaml; coding: utf-8 -*-

    ####
    #### Global parameters
    ####

    # Unfortunately, salt does not support ~ expension
    # We need to use absolute path for `root_dir`
    root_dir: /home/me/.salt
    pki_dir: pki
    cachedir: cache
    sock_dir: run
    pidfile: pids

    log_file: logs/master.log
    key_logfile: logs/key.log

    # Global level logged in file
    log_level_logfile: debug

    ##
    ## file server
    ##
    fileserver_backend:
      - roots

    # Unfortunately, salt does not support ~ expension
    # We need to use absolute path for `file_roots` and `pillar_roots`
    file_roots:
      base:
        - /home/me/.salt/srv/salt/

    pillar_roots:
      base:
        - /home/me/.salt/srv/pillar/

    ####
    #### Salt-SSH specific configuration
    ####
    ssh_log_file: logs/ssh.log
    ssh_use_home_key: True
    ssh_timeout: 5


`roster`_ file declares the list of minions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This file is associate several parameters to a host:

- a ``name`` to use as `target`_
- a ``user``
- use ``sudo`` if the ``user`` is not ``root``

.. code-block:: yaml

    # -*- yaml -*-
    
        forum:
          host: forum.example.net
          user: debian
          sudo: True


.. _SaltStack: https://www.saltstack.com
.. _salt-ssh: https://docs.saltstack.com/en/latest/topics/ssh
.. _additional repository: https://repo.saltstack.com/
.. _bug in salt-ssh that prevent using gitfs: https://github.com/saltstack/salt/issues/23576
.. _salt fileserver: https://docs.saltstack.com/en/latest/ref/configuration/master.html#std:conf_master-fileserver_backend

.. _use salt-ssh as non root user:
.. _use-salt-ssh-non-root: https://docs.saltstack.com/en/latest/topics/ssh/#running-salt-ssh-as-non-root-user

.. _formula documentation: docs/README.rst
.. _states: https://docs.saltstack.com/en/latest/topics/tutorials/starting_states.html
.. _pillars: https://docs.saltstack.com/en/latest/topics/tutorials/pillar.html
.. _roster: https://docs.saltstack.com/en/latest/topics/ssh/index.html#salt-ssh-roster
.. _target: https://docs.saltstack.com/en/latest/topics/ssh/index.html#targeting-with-salt-ssh
.. _discourse: https://www.discourse.org
.. _discourse_docker: https://github.com/discourse/discourse_docker
